# Fashion MNIST

•	This project aimed at classifying the images of clothing like shirts, sneakers.
•	Built and trained a neural network using Tensorflow.
•	Achieved an accuracy of 80% for classifying the images.
